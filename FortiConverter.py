# FortiConverter.py
# This script create an excel equivalent to a .conf file from a FortiGate Equipment for specific xlsx file


# Moreover it can search for information relatives to a specific IP  TODO move this away
#
#
# Requirements : Pandas, openpyxl
#
#
# To execute the script, please enter in CLI :
# FortiConverter.py -f <file.conf>


# Modification 07/11/2019
# Axel BRUN
from forti_lib_ab import *
import sys
import getopt
import pandas as pd
import os


# read the args and main
def main(argv):
    # get command line arguments
    arg_input = 'null'

    try:
        opts, args = getopt.getopt(argv, 'f:')
    except getopt.GetoptError:
        print("args error")
        sys.exit(2)

    for opt, arg in opts:
        if opt == '-f':
            arg_input = arg

    if arg_input == 'null':

        print(""" To execute the script, please enter in CLI :""")
        print(""" FortiConverter.py -f <file.conf> """)


        sys.exit(2)

    elif (arg_input != 'null'):

        if not os.path.isfile(arg_input):
            sys.exit(arg_input + " not found")
        # indicate if the .xlsx is present and ask if generate a new one
        # if excel file is generated or already present, use it for searching IP
        # else use the .conf file

        if arg_input.split(".")[1] == "conf":

            print("\n____CONVERTION TO EXCEL____")
            exists = os.path.isfile("output_" + arg_input.split(".")[0] + ".xlsx")
            if exists:
                print("an excel file is already present")
                if input("generate new excel file ? y/n : ") == "y":
                    generate_excel(arg_input)
                arg_input = "output_" + arg_input.split(".")[0] + ".xlsx"
            else:
                print("no excel file present")
                if input("generate excel file ? y/n : ") == "y":
                    generate_excel(arg_input)
                    arg_input = "output_" + arg_input.split(".")[0] + ".xlsx"

        elif arg_input.split(".")[1] == "xlsx":
            print("\n____CONVERTION TO CONF____")
            exists = os.path.isfile("output_" + arg_input.split(".")[0] + ".conf")
            if exists:
                print("a conf file is already present")
                if input("generate new conf file ? y/n : ") == "y":
                    generate_conf(arg_input)

            else:
                print("no conf file present")
                if input("generate conf file ? y/n : ") == "y":
                    generate_conf(arg_input)
                    print ("done")

    else:
        sys.exit("unexpected argument error")


# creates the excel file by parsing the defined categories
def generate_excel(fichier_conf):
    print("Reading", fichier_conf)
    df_system_interface = parse_categ("system interface", fichier_conf)
    df_firewall_address = parse_categ("firewall address", fichier_conf)
    df_firewall_addrgrp = parse_categ("firewall addrgrp", fichier_conf)
    df_firewall_vip = parse_categ("firewall policy", fichier_conf)
    df_firewall_policy = parse_categ("firewall vip", fichier_conf)
    df_vpn_ipsec_phase1 = parse_categ("vpn ipsec phase1-interface", fichier_conf)
    df_vpn_ipsec_phase2 = parse_categ("vpn ipsec phase2-interface", fichier_conf)

    writer = pd.ExcelWriter("output_" + fichier_conf.split(".")[0] + ".xlsx")

    df_system_interface.to_excel(writer, sheet_name="system interface")
    df_firewall_address.to_excel(writer, sheet_name="firewall address")
    df_firewall_addrgrp.to_excel(writer, sheet_name="firewall addrgrp")
    df_firewall_policy.to_excel(writer, sheet_name="firewall policy")
    df_vpn_ipsec_phase1.to_excel(writer, sheet_name="vpn ipsec phase1-interface")
    df_vpn_ipsec_phase2.to_excel(writer, sheet_name="vpn ipsec phase2-interface")
    df_firewall_vip.to_excel(writer, sheet_name="firewall_vip")
    print("\noutput_" + fichier_conf.split(".")[0] + ".xlsx created")
    writer.save()


# TODO
# creates the .conf file by parsing the defined categories
def generate_conf(fichier_excel):
    # categories
    parts = ["system interface", "firewall address", "firewall addrgrp",
             "firewall policy", "vpn ipsec phase1-interface", "vpn ipsec phase2-interface", "firewall vip"]

    if fichier_excel.split(".")[1] != "xlsx":
        sys.exit(fichier_excel + " is not a .xlsx file")

    df_system_interface = pd.read_excel(fichier_excel, "system interface", index_col=0, dtype=str)
    df_firewall_address = pd.read_excel(fichier_excel, "firewall address", index_col=0, dtype=str)
    df_firewall_addrgrp = pd.read_excel(fichier_excel, "firewall addrgrp", index_col=0, dtype=str)
    df_firewall_policy = pd.read_excel(fichier_excel, "firewall policy", index_col=0, dtype=str)
    df_vpn_ipsec_phase1 = pd.read_excel(fichier_excel, "vpn ipsec phase1-interface", index_col=0, dtype=str)
    df_vpn_ipsec_phase2 = pd.read_excel(fichier_excel, "vpn ipsec phase2-interface", index_col=0, dtype=str)
    df_firewall_vip = pd.read_excel(fichier_excel, "firewall vip", index_col=0, dtype=str)
    with open("output_" + fichier_excel.split(".")[0] + ".conf", "w") as text_file:
        text_file.write("")
    for part in parts:
        generate_part_conf(fichier_excel, part)



# creates a .conf file by parsing a single defined category from an excel file
def generate_part_conf(fichier_excel, part):
    with open("output_" + fichier_excel.split(".")[0] + ".conf", "a") as text_file:
        text_file.write("")

        text_file.write("config " + part)
        text_file.write("\n")
        df = pd.read_excel(fichier_excel, part, index_col=0)
        for index, row in df.iterrows():
            text_file.write("    edit " + str(index).split("\n")[0])
            text_file.write("\n")
            for column in df:
                if str(row[column]) != "nan":
                    text_file.write("        set " + str(column) + " " + str(row[column]) + "\n")
            text_file.write("    next")
            text_file.write("\n")
        text_file.write("end")
        text_file.write("\n")


# parse a specific conf (categ) in a .conf file and return the corresponding df
def parse_categ(categ, file):
    file = open(file, "r")
    in_categ = False
    df = pd.DataFrame()
    categ = "config " + categ
    for line in file:
        # when enter system interface
        if categ == line.split("\n")[0]:
            print(categ)
            print(line)
            in_categ = True
        # when exit
        if "end\n" in line:
            in_categ = False

        if in_categ:

            # when system
            if "    edit " in line:

                list_line = line.split(" ")
                last_row = list_line[-1].split("\n")[0]
            # when parameter
            if "        set" in line:
                list_line = line.split(" ")
                # delete the empty spaces
                del list_line[0:8]
                # join the last fields
                while len(list_line) > 3:
                    join = str(list_line[2]) + " " + str(list_line[3])
                    list_line[2] = join
                    del list_line[3]
                df.at[last_row, list_line[1].split("\n")[0]] = list_line[2].split("\n")[0]
    return df


if __name__ == '__main__':
    main(sys.argv[1:])
print("\nEND OF SCRIPT")