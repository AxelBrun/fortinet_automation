

# check if s is a valid ip X.X.X.X
def is_valid_ip(s):
    a = s.split('.')
    if len(a) != 4:
        return False
    for x in a:
        if not x.isdigit():
            return False
        i = int(x)
        if i < 0 or i > 255:
            return False
    return True


# get a categ and return line list
def get_categ(categ, file):
    """
    :param categ: string
        example : ('firewall policy')
    :param file: string
    :return: string
    """
    file = open(file, "r")
    categ = "config " + categ
    _categ = ""
    in_categ = False
    for line in file:
        # when enter system interface
        if categ == line.split("\n")[0]:
            in_categ = True
        if in_categ:
            _categ += line
        # when exit
        if "end\n" in line and in_categ:
            return _categ

#
# print("\n__________IP SCAN_____________")
# generate_text = input("do you want .txt results ? y/n : ")
# generate_text = generate_text == "y"
#
# search_for_ip = ""
# while search_for_ip != "q":
#
#     if search_for_ip != "":
#         if is_valid_ip(search_for_ip.split(" ")[0]) and is_valid_ip(search_for_ip.split(" ")[1]):
#
#             find_ip(search_for_ip, arg_input, generate_text)
#
#         else:
#             print("not a valid ip")
#             print("use X.X.X.X X.X.X.X format, X in [0;255]")
#     print("\n\npress q to quit")
#     search_for_ip = input("enter the ip you are looking for : ")


# find all rules / groups / objects associated with ip in a .conf or .xlsx file and output a .txt
# def find_ip(ip, file, generate_text):
#     ip = str(ip)
#
#     rules_ip = []
#     obj_ip = []
#     groups_ip = []
#
#     # check the objets corresponding to IP
#     list_obj = scan_by_ip(file, ip)
#     if list_obj == []:
#         print("\nno objects including", ip, "\n")
#     else:
#         obj_ip.extend(list_obj)
#         print("\nobjects including", ip, ":")
#         print(list_obj)
#         if generate_text:
#             with open(ip + ".txt", "w") as text_file:
#                 text_file.write("objects including " + ip + " :\n")
#                 text_file.write(" ; ".join(list_obj))
#
#     for obj in list_obj:
#         # check the rules corresponding to obj
#         list_rules = scan_by_obj_rules(file, obj)
#         if list_rules == []:
#             print("\nno rules direclty including", obj, "\n")
#
#         else:
#             rules_ip.extend(list_rules)
#             print("\nrules including", obj, ":")
#             print(list_rules)
#             if generate_text:
#                 with open(ip + ".txt", "a") as text_file:
#                     text_file.write("\n\nrules including " + obj + " :\n")
#                     text_file.write(" ; ".join(list_rules))
#
#         # check the groups corresponding to obj
#         list_grp = scan_by_obj_groups(file, obj)
#         if list_grp == []:
#             print("\nno groups including", obj, ":")
#         else:
#             groups_ip.extend(list_grp)
#             print("\ngroups including", obj, ":")
#             if generate_text:
#                 with open(ip + ".txt", "a") as text_file:
#                     text_file.write("\n\ngroups including " + obj + " :\n")
#                     text_file.write(" ; ".join(list_grp))
#         print(list_grp)
#
#         for group in list_grp:
#
#             # check the rules corresponding to groups
#             if is_subgroup(file, group, ip, generate_text):
#                 if group not in list_obj:
#                     list_obj.append(group)
#
#             # print(list_obj)
#             else:
#                 list_rules = scan_by_obj_rules(file, group)
#                 if list_rules == []:
#                     print("\nno rules including", obj, ":")
#                 else:
#                     rules_ip.extend(list_rules)
#                     print("\nrules including", group, ":")
#                     print(list_rules)
#                     if generate_text:
#                         with open(ip + ".txt", "a") as text_file:
#                             text_file.write("\n\nrules including " + group + " :\n")
#                             text_file.write(" ; ".join(list_rules))
#
#     print("\n\n___________________________\n\n")
#     print(ip + " is included directly and undirectly in :")
#     print("\nRULES : \n")
#     print(" ; ".join(rules_ip))
#     print("\nOBJECTS : \n")
#     print(" ; ".join(obj_ip))
#     print("\nGROUPS : \n")
#     print(" ; ".join(groups_ip))
#
#     if generate_text:
#         with open(ip + ".txt", "a") as text_file:
#             text_file.write("\n\n___________________________\n")
#             text_file.write(ip + " is directly and undirectly included in :")
#             text_file.write("\nRULES : \n")
#             text_file.write(" ; ".join(rules_ip))
#             text_file.write("\nOBJECTS : \n")
#             text_file.write(" ; ".join(obj_ip))
#             text_file.write("\nGROUPS : \n")
#             text_file.write(" ; ".join(groups_ip))
#         print(ip + ".txt created")



# return the list of obj the ip is associated to in "firewall address" for .conf or .xlsx
# def scan_by_ip(file, ip):
#     print("Searching for ip")
#     result = []
#     # for .conf or .xlsx
#     if file.split(".")[1] == "conf":
#         df_firewall_address = parse_categ("firewall address", file)
#     elif file.split(".")[1] == "xlsx":
#         df_firewall_address = pd.read_excel(file, "firewall address", index_col=0)
#     else:
#         print(file.split(".")[1])
#         sys.exit("file is not a .conf or .xlsx")
#
#     for index, row in df_firewall_address.iterrows():
#         ip_of_df = str(row['subnet']).split("\n")[0]
#         if ip == ip_of_df:
#             # print(ip, "in", index)
#             result.append(str(index).split("\n")[0])
#     return result

#
# # return the list of rules's id including object in "firewall policy" for .conf or .xlsx
# def scan_by_obj_rules(fichier, obj):
#     result = []
#
#     # for .conf or .xlsx
#     if fichier.split(".")[1] == "conf":
#         df_firewall_policy = parse_categ("firewall policy", fichier)
#     elif fichier.split(".")[1] == "xlsx":
#         df_firewall_policy = pd.read_excel(fichier, "firewall policy", index_col=0)
#     else:
#         print(fichier.split(".")[1])
#         sys.exit("file is not a .conf or .xlsx")
#
#     for index, row in df_firewall_policy.iterrows():
#         list_obj_of_df1 = str(row['srcaddr']).split(" ")
#         list_obj_of_df2 = str(row['dstaddr']).split(" ")
#
#         for obj_of_df in list_obj_of_df1:
#             _obj_of_df = obj_of_df.split("\n")[0]
#             if obj == _obj_of_df:
#                 # print(obj, "in rule", index)
#                 result.append(str(index).split("\n")[0])
#
#         for obj_of_df in list_obj_of_df2:
#             _obj_of_df = obj_of_df.split("\n")[0]
#             if obj == _obj_of_df:
#                 # print(obj, "in rule", index.split("\n")[0])
#                 result.append(str(index).split("\n")[0])
#     return result
#
#
# # return the list of groups including object or group in "firewall addrgrp" for .conf or .xlsx
# def scan_by_obj_groups(fichier, obj):
#     result = []
#     obj = obj.split("\n")[0]
#
#     # for .conf or .xlsx
#     if fichier.split(".")[1] == "conf":
#         df_firewall_addrgrp = parse_categ("firewall addrgrp", fichier)
#     elif fichier.split(".")[1] == "xlsx":
#         df_firewall_addrgrp = pd.read_excel(fichier, "firewall addrgrp", index_col=0)
#     else:
#         print(fichier.split(".")[1])
#         sys.exit("file is not a .conf or .xlsx")
#
#     for index, row in df_firewall_addrgrp.iterrows():
#         list_obj_of_df = str(row['member']).split(" ")
#         for obj_of_df in list_obj_of_df:
#             _obj_of_df = obj_of_df.split("\n")[0]
#             if obj == _obj_of_df:
#                 # print(obj, "in group", index)
#                 result.append(str(index).split("\n")[0])
#     return result
#
#
# # check if is a group is part of another group
# def is_subgroup(fichier, group, ip, generate_text):
#     subgroup = False
#     parent = scan_by_obj_groups(fichier, group)
#     if parent != []:
#         if generate_text:
#             with open(ip + ".txt", "a") as text_file:
#                 text_file.write("\n" + group + "is a subgroup of " + ";".join(parent))
#         print(group, "is a subgroup of ", parent)
#         subgroup = True
#     return subgroup

