###
# generate the input to add a new interface to all policies that have a specific interface

# parse jusqu'à config firewall policy
# 	if srcintf "VPN-STQY"
# 		write
# 			edit x
# 				set srcintf "VPN-STQY" "VPN-VDR"
# 	 		next
###
from forti_lib_ab import *

result = ""
file = 'input.conf'


addresses = get_categ('firewall address', file)

output = 'config firewall address\n'
i = 0
while i < len(addresses.splitlines()):
    line = addresses.splitlines()[i]
    # filter the '' in the list
    line = list(filter(''.__ne__, line.split(" ")))
    line_joined = ' '.join(line)
    if line[0] == 'edit':
        last_edit = i
    if line_joined == 'set associated-interface "VPN-STQY"':
        output += addresses.splitlines()[last_edit] + '\n'
        output += '        unset associated-interface \n'
        output += '    next \n'
    i += 1
output += 'end\n'

policies = get_categ('firewall policy', file)

output += 'config firewall policy\n'
i = 0
while i < len(policies.splitlines()):
    line = policies.splitlines()[i]
    # filter the '' in the list
    line = list(filter(''.__ne__, line.split(" ")))
    line_joined = ' '.join(line)
    if line[0] == 'edit':
        last_edit = i
    if line_joined == 'set srcintf "VPN-STQY"':
        output += policies.splitlines()[last_edit] + '\n'
        output += '        set srcintf "VPN-STQY" "VPN-VDR" \n'
        output += '    next \n'
    if line_joined == 'set dstintf "VPN-STQY"':
        output += policies.splitlines()[last_edit] + '\n'
        output += '        set dstintf "VPN-STQY" "VPN-VDR" \n'
        output += '    next \n'

    i += 1
output += 'end'

print(output)

file = open("output.conf",'w')
file.write(output)
file.close()
